# language: pt


Funcionalidade: Login

    Contexto: Realizar login no site phptravels
    Dado que deve acessar o site php travels
    Quando realizar login com 'usuario' e 'senha'
    E acessar a tela de Gestao de responsaveis
    Entao deve validar a lista de nomes de responsaveis